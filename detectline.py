import numpy as np
import cv2
import PIL
import matplotlib.pyplot as plt
import math
import os
import pprint

################ Self Library ################################################################


def calculatecolororder(img):
    ### Need to detect RBR
    ###                    or GBG
    refilledimg = img.copy()
    src_h,src_w = refilledimg.shape[:2]
    for row in range(src_h-1,0,-1):
        precolor = 0 # RGB = 1,2,3
        isGreen = 0
        isBlue = 0
        isRed = 0
        redLength = 0
        blueLength = 0
        greenLength = 0
        getlaned = 0
        rowlane = []

        for col in range(src_w):
            if refilledimg[row][col][0] == 0 and refilledimg[row][col][1] == 0 and refilledimg[row][col][2] == 255:
                isRed +=1
                if precolor == 3:
                    redLength += 1
                    if redLength > 5 and getlaned == 0:
                        getlaned = 1
                elif getlaned == 1 and blueLength>0:
                    rowlane.append(["B:",(blueLength),[row,col-1]])
                    blueLength = 0
                    getlaned = 0
                precolor = 3
            # elif refilledimg[row][col][0] == 0 and refilledimg[row][col][1] == 255 and refilledimg[row][col][2] == 0:
            #     isGreen +=1
            #     if precolor == 2:
            #         greenLength += 1
            #         # if greenLength > 3 and getlaned == 0:
            #         #     getlaned = 1
            #     precolor = 2
            elif refilledimg[row][col][0] == 255 and refilledimg[row][col][1] == 0 and refilledimg[row][col][2] == 0:
                isBlue +=1
                if precolor == 1:
                    blueLength += 1
                    if blueLength > 5 and getlaned == 0:
                        getlaned = 1
                elif getlaned == 1 and redLength > 0:
                    rowlane.append(["R:",(redLength),[row,col-1]])
                    redLength = 0
                    getlaned = 0
                precolor = 1
            else:
                if getlaned == 1:
                    if redLength > 0:
                        rowlane.append(["R:",(redLength),[row,col-1]])
                    if blueLength > 0:
                        rowlane.append(["B:",(blueLength),[row,col-1]])

                    redLength = 0
                    blueLength = 0
                    getlaned = 0
                pass

            if col==src_w-1:
                if getlaned == 1:
                    if redLength > 0:
                        rowlane.append(["R:",(redLength),[row,col-1]])
                    if blueLength > 0:
                        rowlane.append(["B:",(blueLength),[row,col-1]])

                    redLength = 0
                    blueLength = 0
                    getlaned = 0
                pass                
            #print refilledimg[row][col]
            pass
        #print "row:", row ,"isRed = ",isRed, "isGreen = ",isGreen, "isBlue = ",isBlue
        print "row:", row ,"isRed = ",isRed, "isBlue = ",isBlue, 
        pprint.pprint(rowlane)

    h, w = refilledimg.shape[:2]
    mask = np.zeros([h+2, w+2], np.uint8)
    for r in rowlane:
        cv2.floodFill(refilledimg, mask, (r[2][1],r[2][0]), (255, 0, 0), (100, 100, 50), (50, 50, 50), cv2.FLOODFILL_FIXED_RANGE)

    return refilledimg
########  red to blue might be OK....... noooooooooooooooooooo~
########  green blue green need another 

def translate(src,translate_x,translate_y):
    src_h,src_w = src.shape[:2]
    dst = np.zeros(src.shape,dtype=np.uint8)
    for row in range(src_h):
        for col in range(src_w):
            h = int(row-translate_y)
            w = int(col-translate_x)
            if h<src_h and h>=0 and w<src_w and w>=0:
                dst[row][col] = src[h][w]

################ Load Library ################################################################

def logit(msg):
    print("%s" % msg)

def rad2deg(radius):
    return radius / np.pi * 180.0

def deg2rad(degree):
    return degree / 180.0 * np.pi

def bgr2rgb(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

def _normalize_brightness(img):
    maximum = img.max()
    if maximum == 0:
        return img
    adjustment = min(255.0/img.max(), 3.0)
    normalized = np.clip(img * adjustment, 0, 255)
    normalized = np.array(normalized, dtype=np.uint8)
    return normalized

def _flatten_rgb(img):
    r, g, b = cv2.split(img)
    r_filter = (r == np.maximum(np.maximum(r, g), b)) & (r >= 120) & (g < 150) & (b < 150)
    g_filter = (g == np.maximum(np.maximum(r, g), b)) & (g >= 120) & (r < 150) & (b < 150)
    b_filter = (b == np.maximum(np.maximum(r, g), b)) & (b >= 120) & (r < 150) & (g < 150)
    y_filter = ((r >= 128) & (g >= 128) & (b < 100))

    r[y_filter], g[y_filter] = 255, 255
    b[np.invert(y_filter)] = 0

    b[b_filter], b[np.invert(b_filter)] = 255, 0
    r[r_filter], r[np.invert(r_filter)] = 255, 0
    g[g_filter], g[np.invert(g_filter)] = 255, 0

    flattened = cv2.merge((r, g, b))
    return flattened

def _crop_image(img):
    bottom_half_ratios = (0.47, 1.0)
    bottom_half_slice  = slice(*(int(x * img.shape[0]) for x in bottom_half_ratios))
    bottom_half        = img[bottom_half_slice, :, :]
    return bottom_half



def preprocess(img):
    img = _crop_image(img)
    #img = _normalize_brightness(img)
    img = _flatten_rgb(img)
    return img



def find_lines(img):
    grayed      = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred     = cv2.GaussianBlur(grayed, (3, 3), 0)
    #edged      = cv2.Canny(blurred, 0, 150)

    sobel_x     = cv2.Sobel(blurred, cv2.CV_16S, 1, 0)
    sobel_y     = cv2.Sobel(blurred, cv2.CV_16S, 0, 1)
    sobel_abs_x = cv2.convertScaleAbs(sobel_x)
    sobel_abs_y = cv2.convertScaleAbs(sobel_y)
    edged       = cv2.addWeighted(sobel_abs_x, 0.5, sobel_abs_y, 0.5, 0)

    lines       = cv2.HoughLinesP(edged, 1, np.pi / 180, 10, 5, 5)
    return lines



def _find_best_matched_line(thetaA0, thetaB0, tolerance, vectors, matched = None, start_index = 0):
    if matched is not None:
        matched_distance, matched_length, matched_thetaA, matched_thetaB, matched_coord = matched
        matched_angle = abs(np.pi/2 - matched_thetaB)

    for i in xrange(start_index, len(vectors)):
        distance, length, thetaA, thetaB, coord = vectors[i]

        if (thetaA0 is None or abs(thetaA - thetaA0) <= tolerance) and \
           (thetaB0 is None or abs(thetaB - thetaB0) <= tolerance):
            
            if matched is None:
                matched = vectors[i]
                matched_distance, matched_length, matched_thetaA, matched_thetaB, matched_coord = matched
                matched_angle = abs(np.pi/2 - matched_thetaB)
                continue

            heading_angle = abs(np.pi/2 - thetaB)

            if heading_angle > matched_angle:
                continue
            if heading_angle < matched_angle:
                matched = vectors[i]
                matched_distance, matched_length, matched_thetaA, matched_thetaB, matched_coord = matched
                matched_angle = abs(np.pi/2 - matched_thetaB)
                continue
            if distance < matched_distance:
                continue
            if distance > matched_distance:
                matched = vectors[i]
                matched_distance, matched_length, matched_thetaA, matched_thetaB, matched_coord = matched
                matched_angle = abs(np.pi/2 - matched_thetaB)
                continue
            if length < matched_length:
                continue
            if length > matched_length:
                matched = vectors[i]
                matched_distance, matched_length, matched_thetaA, matched_thetaB, matched_coord = matched
                matched_angle = abs(np.pi/2 - matched_thetaB)
                continue

    return matched



def find_steering_angle_by_line(img, last_steering_angle, debug = True):
    steering_angle = 0.0
    lines          = find_lines(img)

    if lines is None:
        return steering_angle

    image_height = img.shape[0]
    image_width  = img.shape[1]
    camera_x     = image_width / 2
    camera_y     = image_height
    vectors      = []

    for line in lines:
        for x1, y1, x2, y2 in line:
            thetaA   = math.atan2(abs(y2 - y1), (x2 - x1))
            thetaB1  = math.atan2(abs(y1 - camera_y), (x1 - camera_x))
            thetaB2  = math.atan2(abs(y2 - camera_y), (x2 - camera_x))
            thetaB   = thetaB1 if abs(np.pi/2 - thetaB1) < abs(np.pi/2 - thetaB2) else thetaB2

            length   = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            distance = min(math.sqrt((x1 - camera_x) ** 2 + (y1 - camera_y) ** 2),
                           math.sqrt((x2 - camera_x) ** 2 + (y2 - camera_y) ** 2))

            vectors.append((distance, length, thetaA, thetaB, (x1, y1, x2, y2)))

            if debug:
                # draw the edges
                cv2.line(img, (x1, y1), (x2, y2), (255, 255, 0), 2)

    #the line of the shortest distance and longer length will be the first choice
    vectors.sort(lambda a, b: cmp(a[0], b[0]) if a[0] != b[0] else -cmp(a[1], b[1]))

    best = vectors[0]
    best_distance, best_length, best_thetaA, best_thetaB, best_coord = best
    tolerance = np.pi / 180.0 * 10.0

    best = _find_best_matched_line(best_thetaA, None, tolerance, vectors, matched = best, start_index = 1)
    best_distance, best_length, best_thetaA, best_thetaB, best_coord = best

    if debug:
        #draw the best line
        cv2.line(img, best_coord[:2], best_coord[2:], (0, 255, 255), 2)

    if abs(best_thetaB - np.pi/2) <= tolerance and abs(best_thetaA - best_thetaB) >= np.pi/4:
        print "*** sharp turning"
        best_x1, best_y1, best_x2, best_y2 = best_coord
        f = lambda x: int(((float(best_y2) - float(best_y1)) / (float(best_x2) - float(best_x1)) * (x - float(best_x1))) + float(best_y1))
        left_x , left_y  = 0, f(0)
        right_x, right_y = image_width - 1, f(image_width - 1)

        if left_y < right_y:
            best_thetaC = math.atan2(abs(left_y - camera_y), (left_x - camera_x))

            if debug:
                #draw the last possible line
                cv2.line(img, (left_x, left_y), (camera_x, camera_y), (255, 128, 128), 2)
                cv2.line(img, (left_x, left_y), (best_x1, best_y1), (255, 128, 128), 2)
        else:
            best_thetaC = math.atan2(abs(right_y - camera_y), (right_x - camera_x))

            if debug:
                #draw the last possible line
                cv2.line(img, (right_x, right_y), (camera_x, camera_y), (255, 128, 128), 2)
                cv2.line(img, (right_x, right_y), (best_x1, best_y1), (255, 128, 128), 2)

        steering_angle = best_thetaC
    else:
        steering_angle = best_thetaB

    if (steering_angle - np.pi/2) * (last_steering_angle - np.pi/2) < 0:
        last = _find_best_matched_line(None, last_steering_angle, tolerance, vectors)

        if last:
            last_distance, last_length, last_thetaA, last_thetaB, last_coord = last
            steering_angle = last_thetaB

            if debug:
                #draw the last possible line
                cv2.line(img, last_coord[:2], last_coord[2:], (255, 128, 128), 2)

    if debug:
        #draw the steering direction
        r = 60
        x = image_width / 2 + int(r * math.cos(steering_angle))
        y = image_height    - int(r * math.sin(steering_angle))
        cv2.line(img, (image_width / 2, image_height), (x, y), (255, 0, 255), 2)
        logit("line angle: %0.2f, steering angle: %0.2f, last steering angle: %0.2f" % (rad2deg(best_thetaA), rad2deg(np.pi/2-steering_angle), rad2deg(np.pi/2-last_steering_angle)))

    return (np.pi/2 - steering_angle)



def find_steering_angle_by_color(img, last_steering_angle, debug = True):
    r, g, b      = cv2.split(img)
    image_height = img.shape[0]
    image_width  = img.shape[1]
    camera_x     = image_width / 2
    image_sample = slice(0, int(image_height * 0.2))
    sr, sg, sb   = r[image_sample, :], g[image_sample, :], b[image_sample, :]
    track_list   = [sr, sg, sb]
    tracks       = map(lambda x: len(x[x > 20]), [sr, sg, sb])
    tracks_seen  = filter(lambda y: y > 50, tracks)

    if len(tracks_seen) == 0:
        return 0.0

    maximum_color_idx = np.argmax(tracks, axis=None)
    _target = track_list[maximum_color_idx]
    _y, _x = np.where(_target == 255)
    px = np.mean(_x)
    steering_angle = math.atan2(image_height, (px - camera_x))

    if debug:
        #draw the steering direction
        r = 60
        x = image_width / 2 + int(r * math.cos(steering_angle))
        y = image_height    - int(r * math.sin(steering_angle))
        cv2.line(img, (image_width / 2, image_height), (x, y), (255, 0, 255), 2)
        cv2.circle(img,(int(px),0), 8, (255,255,255), -1)
        logit("steering angle: %0.2f, last steering angle: %0.2f" % (rad2deg(steering_angle), rad2deg(np.pi/2-last_steering_angle)))

    return (np.pi/2 - steering_angle) * 2.0

##############################################################################################


#imgfname = "sample.png"

imgfname = "a.jpg"
src_img = cv2.imread(imgfname, cv2.IMREAD_COLOR)
processed_img = preprocess(src_img)

refilled_img = calculatecolororder(processed_img)


find_steering_angle_by_color(processed_img, 0)

gray_scale_img = cv2.cvtColor(processed_img, cv2.COLOR_BGR2GRAY)

find_steering_angle_by_color(refilled_img, 0)


while 1:
    cv2.imshow("src_img", src_img)
    cv2.imshow("processed_img", processed_img)

    cv2.imshow("gray_scale_img", gray_scale_img)
    #cv2.imshow("edge_img", edge_img)

    cv2.imshow("refilled_img", refilled_img)

    cv2.waitKey(1)	
    k = cv2.waitKey(5000)

    if k == 113:         # If 'q' was pressed exit
        cv2.destroyAllWindows()
        break


"""
r, g, b = cv2.split(processed_img)
r_filter = (r == np.maximum(np.maximum(r, g), b)) & (r >= 120) & (g < 150) & (b < 150)
g_filter = (g == np.maximum(np.maximum(r, g), b)) & (g >= 120) & (r < 150) & (b < 150)
b_filter = (b == np.maximum(np.maximum(r, g), b)) & (b >= 120) & (r < 150) & (g < 150)
# r_filter = (r == np.maximum(np.maximum(r, g), b))
# g_filter = (g == np.maximum(np.maximum(r, g), b))
# b_filter = (b == np.maximum(np.maximum(r, g), b))
y_filter = ((r >= 128) & (g >= 128) & (b < 100))

r[y_filter], g[y_filter] = 255, 255
b[np.invert(y_filter)] = 0

b[b_filter], b[np.invert(b_filter)] = 255, 0
r[r_filter], r[np.invert(r_filter)] = 255, 0
g[g_filter], g[np.invert(g_filter)] = 255, 0

mono_tone = cv2.merge((r, g, b))
gray_scale_img = cv2.cvtColor(mono_tone, cv2.COLOR_RGB2GRAY)
h, w = gray_scale_img.shape
gray_scale_img[:h,:] = 0
edge_img = cv2.Canny(gray_scale_img, 50, 150)
lines = cv2.HoughLinesP(edge_img, 1, np.pi/180, 50)

fig, ax = plt.subplots()
ax.imshow(src_img)
"""



'''
tol=0.1 # Tolerance value for filter out the horizon lines
for l in lines:
    x1,y1,x2,y2 = l[0]
    theta = math.atan2(abs(x1-x2), abs(y1-y2)) # using atan to find the angle
    if theta>np.pi/2-tol: continue  # Remove the horizon lines
    ax.plot([x1,x2],[y1,y2],'r')

plt.show
'''