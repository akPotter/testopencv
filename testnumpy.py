import numpy as np
import cv2
import matplotlib.pyplot as plt
import math
import os
import pprint

def flatten_rgb(img):
    r, g, b = cv2.split(img)
    r_filter = (r == np.maximum(np.maximum(r, g), b)) & (r >= 120) & (g < 150) & (b < 150)
    g_filter = (g == np.maximum(np.maximum(r, g), b)) & (g >= 120) & (r < 150) & (b < 150)
    b_filter = (b == np.maximum(np.maximum(r, g), b)) & (b >= 120) & (r < 150) & (g < 150)
    y_filter = ((r >= 128) & (g >= 128) & (b < 100))

    r[y_filter], g[y_filter] = 255, 255
    b[np.invert(y_filter)] = 0

    b[b_filter], b[np.invert(b_filter)] = 255, 0
    r[r_filter], r[np.invert(r_filter)] = 255, 0
    g[g_filter], g[np.invert(g_filter)] = 255, 0

    flattened = cv2.merge((r, g, b))
    return flattened

def crop_image(img):
    bottom_half_ratios = (0.55, 1.0)
    bottom_half_slice  = slice(*(int(x * img.shape[0]) for x in bottom_half_ratios))
    bottom_half        = img[bottom_half_slice, :, :]
    return bottom_half

def preprocess(img):
    img = crop_image(img)
    #img = ImageProcessor._normalize_brightness(img)
    img = flatten_rgb(img)
    return img


imgfname = "direct.jpg"

ori_img = cv2.imread(imgfname, cv2.IMREAD_COLOR)
img = preprocess(ori_img)

r, g, b      = cv2.split(img)

pprint.pprint(r)
#print g
#print b

image_height = img.shape[0]
image_width  = img.shape[1]
camera_x     = image_width / 2
image_sample = slice(0, int(image_height * 0.2))
sr, sg, sb   = r[image_sample, :], g[image_sample, :], b[image_sample, :]
track_list   = [sr, sg, sb]

pprint.pprint(track_list)
print "======================================================================"

tracks       = map(lambda x: len(x[x > 20]), [sr, sg, sb])
tracks_seen  = filter(lambda y: y > 50, tracks)

print "[maximum_color_idx]======================================================================"
maximum_color_idx = np.argmax(tracks, axis=None)
pprint.pprint(maximum_color_idx)

_target = track_list[maximum_color_idx]

print "[_target]======================================================================"
pprint.pprint(_target)
_y, _x = np.where(_target == 255)

print "[_x]======================================================================"
pprint.pprint(_x)
print "[_y]======================================================================"
pprint.pprint(_y)

px = np.mean(_x)
steering_angle = math.atan2(image_height, (px - camera_x))