# Require imagemagick
# require potrace 
# sudo apt-get install imagemagick potrace
import os
import argparse
import glob

parser = argparse.ArgumentParser()
parser.add_argument(
    "files", type=str, help="png2svg", nargs="*"
)

parser.add_argument(
    "-o", "--output", type=str, help="folder", default="./svgs/"
)


args = parser.parse_args()

for f in args.files:
    print f
    pnmname =  f.split(".")[0] + ".pnm"
    svgname =  os.path.basename(f).split(".")[0] + ".svg"
    os.system("convert %s %s" % (f, pnmname))
    if not os.path.exists(args.output):
        os.mkdir(args.output)
    svgname = os.path.join(args.output, svgname)
    os.system("potrace -s -o %s %s" % (svgname, pnmname))
    os.remove(pnmname)