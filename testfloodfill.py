# -*- coding=GBK -*-
import cv2 as cv
import numpy as np


def _crop_image(img):
    bottom_half_ratios = (0.47, 1.0)
    bottom_half_slice  = slice(*(int(x * img.shape[0]) for x in bottom_half_ratios))
    bottom_half        = img[bottom_half_slice, :, :]
    return bottom_half 

def fill_image(image):
    global croppedimage_height, croppedimage_width
    copyImage = image.copy()
    h, w = image.shape[:2]
    mask = np.zeros([h+2, w+2], np.uint8)
    cv.floodFill(copyImage, mask, (croppedimage_width/2-1, croppedimage_height-1), (204, 0, 204), (100, 100, 50), (50, 50, 50), cv.FLOODFILL_FIXED_RANGE)
    cv.imshow("filled", copyImage)

srcimg = cv.imread("a.jpg")
cv.imshow("srcimg", srcimg)

cropped = _crop_image(srcimg)
croppedimage_height = cropped.shape[0]
croppedimage_width  = cropped.shape[1]
print cropped.shape

cv.imshow("cropped", cropped)
fill_image(cropped)
cv.waitKey(0)
